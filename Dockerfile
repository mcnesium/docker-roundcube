FROM roundcube/roundcubemail:latest

RUN set -ex; \
    apt-get update; \
    apt-get install -y --no-install-recommends \
        git \
    ; \
    \
    curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/bin --filename=composer; \
    mv /usr/src/roundcubemail/composer.json-dist /usr/src/roundcubemail/composer.json; \
    \
    composer config --file=/usr/src/roundcubemail/composer.json minimum-stability dev;

RUN composer \
        --working-dir=/usr/src/roundcubemail/ \
        --prefer-dist --prefer-stable \
        --no-update --no-interaction \
        --optimize-autoloader --apcu-autoloader \
        require \
            roundcube/carddav \
            johndoh/contextmenu:=3.0 \
            elm/identity_smtp:=dev-master \
    ; \
    composer \
        --working-dir=/usr/src/roundcubemail/ \
        --prefer-dist \
        --no-interaction \
        --optimize-autoloader --apcu-autoloader \
        update;

RUN mkdir -p /etc/enigma/gnupg \
 && chown www-data:www-data /etc/enigma/gnupg \
 && cp /usr/src/roundcubemail/plugins/enigma/config.inc.php.dist /usr/src/roundcubemail/plugins/enigma/config.inc.php \
 && echo "\$config['enigma_pgp_homedir'] = '/etc/enigma/gnupg';" >> /usr/src/roundcubemail/plugins/enigma/config.inc.php
